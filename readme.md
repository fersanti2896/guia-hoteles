# Proyecto. Guía de Hoteles

#### RESUMEN MÓDULO 1

Este es el proyecto _Curso-DWB_ que cuenta con un repositorio: **guia-bicicletas** que corresponde a la _Módulo 1_.

Se detalla todos los elementos que componen a este proyecto, como la integración de Bootstrap

#### DESARROLLO MÓDULO 1

**1. Archivo .ignore**

Nuestro archivo _ignore_ contiene las siguientes instrucciones:

![punto1](images/ignore.PNG)

**2. Scrip dev sobre el package.json**

Nuestro script _package.js_ donde viene nuestro lite-server:

![punto2](images/lite-server.PNG)

**3. Referencias CSS, JS a Bootstap, Jquery y Popper**

En nuestro archivo _index.html_ tenemos las siguientes referencias necesarias para Bootstrap, JQuery y Popper.

![punto3](images/referencias.PNG)

**4. Container con 3 parráfos y un título como descripción**

Para esto, usamos el siguiente código:

    <div class="container">
     <div class="row">
            <div class="col-sm">
                <h3 class="display-6">¿Cómo funciona?</h3>
                <p class="lead">Busca los hoteles de acuerdo a tu preferencia.</p>
            </div>
            <div class="col-sm">
                <h3 class="display-6">¿Dónde puedo reservar?</h3>
                <p class="lead">Elige la ciudad que te interese y luego realiza la reservación con tus datos.</p>
            </div>
            <div class="col-sm">
                <h3 class="display-6">¿Cómo pago?</h3>
                <p class="lead">Tenemos varias formas de pago.</p>
            </div>
        </div>
    </div>

En nuestra vista _index_ tenemos: 

![punto4](images/parrafos.PNG)

**5. Lista de productos con título, descripción y un botón**

Para esto usamos el siguiente código en nuestro archivo _index.html_ con algunos productos el cual incluye su título, descripción y un botón:

    <div class="card-hotel d-flex flex-column">
        <div>
            <h4 class="display-6">Hotel Barcelona</h4>
            <p class="lead">Hotel ubicado en pleno centro de Barcelona a poco metros de la playa.</p>
        </div>
        <div><button class="btn btn-info btn-reserva">Reservar</button></div>
        </div>
        <div class="card-hotel d-flex flex-column">
            <div>
                <h4 class="display-6">Hotel México</h4>
                <p class="lead">Hotel ubicado en playa del carmen.</p>
            </div>
        <div><button class="btn btn-info btn-reserva">Reservar</button></div>
        </div>
        <div class="card-hotel d-flex flex-column">
            <div>
                <h4 class="display-6">Hotel California</h4>
                <p class="lead">Hotel ubicado en los Ángeles de California.</p>
            </div>
            <div><button class="btn btn-info btn-reserva">Reservar</button></div>
        </div>

Visualmente obtenemos: 

![punto5](images/productos.PNG)

**6. Sistema de grillas con flex**

Sistema de grillas donde se ubicaron los productos transformados en flex, con el siguiente código se extableció el sistema de grillas:

    <div class="d-flex flex-wrap">
        ...
    </div>

Se puede referencia a la imagen del punto 5, donde se implementa dicho flex. 

**7. Sección de footwer con información comercial**

Se usó el siguiente código para implementar la parte de footer: 

    <footer class="footer">
        <div class="row">
            <div class="col-sm-4 d-flex flex-column">
                <a href="#">Facebook</a>
                <a href="#">Twitter</a>
                <a href="#">Instagram</a>
            </div>
            <div class="col-sm-4 d-flex flex-column">
                <address>
                    <h5>Oficina Central</h5>
                    <p> Tláhuac, CDMX
                        +324567456</p>
                </address>
            </div>
            <div class="col-sm-4 d-flex flex-column">
                <a href="#">Nosotros</a>
                <a href="#">Precios</a>
                <a href="#">Términos y Condiciones</a>
                <a href="#">Contacto</a>
            </div>
        </div>
    </footer>

Visualmente obtenemos: 

![punto6](images/footer.PNG)

**8. Dirección comercial**

Para la parte de dirección comercial, se usó dicho código: 

    <address>
        <h5>Oficina Central</h5>
        <p> Tláhuac, CDMX +324567456</p>
    </address>    

**9. Archivo CSS**

Se crea el siguiente archivo con el código fuente: 

![punto7](images/estilos.PNG)

**10. Clases de control de alineación**

En el archivo _main.css_ del punto 9 se detalla el control de alineación para ubicar los elementos del footer. 

#### CONCLUSIONES MÓDULO 1

Como se puede observar, se ha llegado al objetivo del módulo, donde vimos cómo crear un proyecto, subirlo a _Bitbucket_, crear nuestro servidor e ir añadiendo las referencias para darle estilos a nuestra plataforma web.

Visualmente nuestra plataforma se ve de la siguiente forma: 

![conclusion1](images/modulo1.PNG)
